import State from "./state";
import { parseQuery, restoreOperationsFromQuery, color } from "./utils";
import {
  createShareButtonClickListener,
  createOnPlusListener,
  createOnMinusListener,
  createOnTimeUpdateListener,
  createOnVideoEndListener,
  createOnChangeListener,
  createOnVideoCanPlayListener
} from "./eventListeners";

// setTimeout(createOnVideoEndListener(State), 5000)
State.dom.video.addEventListener("ended", createOnVideoEndListener(State));
State.dom.plus.onclick = createOnPlusListener(State);
State.dom.minus.onclick = createOnMinusListener(State);
State.dom.video.crossOrigin = "Anonymous";
State.dom.modalButton.onclick = () => {
  State.dom.modalContainer.style.opacity = 0;
  setTimeout(() => {
    State.dom.video.play();
    State.dom.modalContainer.style.display = "none";
  }, 1000);
};

State.dom.slider.addEventListener("change", createOnChangeListener(State));

State.dom.sharedModalButton.onclick = () => {
  State.dom.sharedModalContainer.style.opacity = 0;

  setTimeout(() => {
    State.dom.video.play();
    State.dom.sharedModalContainer.style.display = "none";
  }, 1000);
};

State.dom.video.onclick = e => {
  if (State.pause) {
    State.dom.video.play();
    State.pause = false;
  } else {
    State.dom.video.pause();
    State.pause = true;
  }
}

document.addEventListener('keypress', e => {
  if (e.keyCode !== 32) {
    return;
  }

  if (State.pause) {
    State.dom.video.play();
    State.pause = false;
  } else {
    State.dom.video.pause();
    State.pause = true;
  }
})

State.dom.shareSiteButton.onclick = e => {
  State.dom.shareSiteButton.style.display = 'none';
  State.dom.vk2.style.display = 'block';
  State.dom.fb2.style.display = 'block';
  State.dom.url2.style.display = 'block';
  State.dom.vk2.onclick = e => {
    window.location.replace(
      "http://vk.com/share.php?url=" + encodeURIComponent(window.location.origin),
      window
    );
  };
  State.dom.fb2.onclick = e => {
    window.location.replace(
      "https://www.facebook.com/sharer.php?u=" + encodeURIComponent(window.location.origin),
      window
    );
  };
  State.dom.url2.value = window.location.origin;
}

if (window.location.search) {
  State.dom.modalContainer.style.display = "none";
  State.dom.sharedModalContainer.style.display = "flex";
  State.dom.plus.style.display = "none";
  State.dom.minus.style.display = "none";

  State.operations = restoreOperationsFromQuery(State.query);

  State.dom.video.addEventListener(
    "timeupdate",
    createOnTimeUpdateListener(State)
  );

  const passMyOwnRow = document.getElementById("pass-my-own");
  const passMyOwn = document.getElementById("start-own");

  passMyOwnRow.style.display = "flex";
  passMyOwn.addEventListener("click", () =>
    window.location.replace(window.location.origin, window)
  );
}

function drawCanv() {
  if (State.w && State.h) {
    State.dom.canvTmpContext.drawImage(State.dom.video, 0, 0, State.w, State.h);

    const pixelData = State.dom.canvTmpContext.getImageData(
      0,
      0,
      State.w,
      State.h
    );

    for (let i = 0; i < pixelData.data.length; i++) {
      if (i % 4 === 3) continue;
      let updatedValue = pixelData.data[i] + State.scale;

      pixelData.data[i] = color(updatedValue);
    }

    State.dom.canvOutContext.putImageData(pixelData, 0, 0);
  }

  requestAnimationFrame(drawCanv);
}

// drawCanv();
