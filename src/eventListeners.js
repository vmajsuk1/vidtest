import { createQueryFromState } from "./utils";

export const createShareButtonClickListener = State => {
  return () => {
    const qry = createQueryFromState(State);
    const url = window.location.origin + qry;

    State.dom.sliderRow.style.display = "none";
    State.dom.shareButtonsRow.style.marginTop = "30";
    State.dom.urlInputRow.style.display = "flex";
    State.dom.urlInput.value = url;

    State.dom.socialNetworksRow.style.display = "flex";
    State.dom.vk.onclick = () =>
      window.location.replace(
        "http://vk.com/share.php?url=" + encodeURIComponent(url),
        window
      );
    State.dom.fb.onclick = () =>
      window.location.replace(
        "https://www.facebook.com/sharer.php?u=" + encodeURIComponent(url),
        window
      );

    console.log("??");

    State.dom.urlInput.select();
  };
};

export const createOnPlusListener = State => {
  return () => {
    State.operations.push({
      time: State.dom.video.currentTime,
      operation: "plus"
    });
    State.plus();
  };
};

export const createOnMinusListener = State => {
  return () => {
    State.operations.push({
      time: State.dom.video.currentTime,
      operation: "minus"
    });
    State.minus();
  };
};

export const createOnChangeListener = State => {
  return e => {
    const scale = 100 - 2 * e.target.value;

    State.operations.push({
      time: State.dom.video.currentTime,
      operation: "change",
      value: scale
    });
    State.change(scale);
  };
};

export const createOnTimeUpdateListener = State => {
  return () => {
    let i = 0,
      n = State.operations.length - 1,
      curTime = State.dom.video.currentTime;

    if (n < 0) return;

    for (; i < n; i++) {
      if (
        curTime >= State.operations[i].time &&
        curTime < State.operations[i + 1].time
      ) {
        switch (State.operations[i].operation) {
          case "plus":
            State.plus();
            break;
          case "minus":
            State.minus();
            break;
          case "change":
            State.change(State.operations[i].value);
            break;
        }

        State.operations.splice(i, 1);
      }
    }

    let newN = State.operations.length - 1;

    if (curTime >= State.operations[newN].time) {
      State.operations[newN].operation === "plus"
        ? State.plus()
        : State.minus();
      State.operations.splice(newN, 1);
    }
  };
};

export const createOnVideoEndListener = State => {
  return () => {
    if (!window.location.search) {
      State.dom.shareSite.style.display = 'none';
      State.dom.buttonsRow.style.display = "none";
      State.dom.shareButtonsRow.style.display = "flex";
      State.dom.shareButton.addEventListener(
        "click",
        createShareButtonClickListener(State)
      );
      State.dom.watch.addEventListener("click", () => {
        State.allOperations = [].concat(
          State.allOperations || State.operations
        );
        State.operations = [].concat(State.allOperations);

        State.scale = 0;
        State.dom.video.currentTime = 0;
        State.dom.video.addEventListener(
          "timeupdate",
          createOnTimeUpdateListener(State)
        );
        State.recalc();
        State.dom.video.play();
      });
    }
  };
};

export const createOnVideoCanPlayListener = State => {
  return () => {
    State.dom.video.play();
  };
};
