export function color(c) {
  if (c > 255) return 255;
  if (c < 0) return 0;
  return c;
}

export function parseQuery(queryStr) {
  if (!queryStr) {
    return {};
  }

  let kvs = queryStr.slice(1).split("&");
  let res = {};
  let i = 0,
    n = kvs.length;

  for (; i < n; i++) {
    let tmp = kvs[i].split("=");
    res[tmp[0]] = tmp[1];
  }

  return res;
}

export function restoreOperationsFromQuery(query) {
  const operations = [];

  if (query.plus) {
    let pluses = query.plus.split(","),
      i = 0,
      n = pluses.length;

    for (; i < n; i++) {
      operations.push({
        operation: "plus",
        time: pluses[i]
      });
    }
  }

  if (query.minus) {
    let minuses = query.minus.split(","),
      i = 0,
      n = minuses.length;

    for (; i < n; i++) {
      operations.push({
        operation: "minus",
        time: minuses[i]
      });
    }
  }

  if (query.change) {
    let changes = query.change.split(","),
      i = 0,
      n = changes.length;

    for (; i < n; i++) {
      const change = changes[i].split("!");
      const time = change[0];
      const value = change[1];
      operations.push({
        operation: "change",
        time,
        value
      });
    }
  }

  operations.sort((a, b) => a.time - b.time);

  return operations;
}

export function createQueryFromState(State) {
  let operations = State.allOperations || State.operations;
  let pluses = operations.filter(o => o.operation === "plus").map(o => o.time);
  let minuses = operations
    .filter(o => o.operation === "minus")
    .map(o => o.time);
  let changes = operations.filter(o => o.operation === "change");
  let qryParts = [];
  if (pluses.length > 0) {
    qryParts.push("plus=" + pluses.join(","));
  }
  if (minuses.length > 0) {
    qryParts.push("minus=" + minuses.join(","));
  }
  if (changes.length > 0) {
    qryParts.push(
      "change=" + changes.map(c => c.time + "!" + c.value).join(",")
    );
  }
  let qry = qryParts.length > 0 ? "?" + qryParts.join("&") : "";

  return qry;
}
