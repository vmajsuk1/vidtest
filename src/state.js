import { parseQuery } from "./utils";

const video = document.getElementById("vid");
const root = document.getElementById("root");
const plus = document.getElementById("plus");
const minus = document.getElementById("minus");
const buttonsRow = document.getElementById("buttons-row");
const shareButtonsRow = document.getElementById("share-buttons-row");
const shareButton = document.getElementById("share");
const urlInputRow = document.getElementById("url-row");
const urlInput = document.getElementById("url");
const slider = document.getElementById("slider");
const modalButton = document.getElementById("modal-button");
const sharedModalButton = document.getElementById("shared-modal-button");
const modalContainer = document.getElementById("modal-container");
const sharedModalContainer = document.getElementById("shared-modal-container");
const socialNetworksRow = document.getElementById("social-networks-row");
const vk = document.getElementById("vk");
const fb = document.getElementById("fb");
const sliderRow = document.getElementById("slider-row");
const watch = document.getElementById("watch");
const shareSiteButton = document.getElementById("share-site-button");
const vk2 = document.getElementById("vk-2");
const fb2 = document.getElementById("fb-2");
const url2 = document.getElementById("url-2");
const shareSite = document.getElementById("share-site");

const State = {
  scale: 0,
  w: 0,
  h: 0,
  operations: [],
  pause: false,
  dom: {
    video,
    root,
    plus,
    minus,
    buttonsRow,
    shareButtonsRow,
    shareButton,
    urlInputRow,
    urlInput,
    slider,
    modalButton,
    sharedModalButton,
    modalContainer,
    sharedModalContainer,
    socialNetworksRow,
    vk,
    fb,
    sliderRow,
    watch,
    shareSiteButton,
    vk2,
    fb2,
    url2,
    shareSite
  },
  query: parseQuery(window.location.search)
};

function grScale() {
  if (State.scale === 0) {
    return 30;
  } else if (State.scale > 0) {
    return 30 - State.scale / 100 * 30;
  } else if (State.scale < 0) {
    return 30 - State.scale / 100 * 70;
  }
}

function backgroundColor(scale) {
  if (scale >= 0) {
    return 244;
  } else {
    return 244 + scale;
  }
}

State.plus = () => {
  if (State.scale < 100) {
    State.scale += 10;
    State.recalc();
  }
};

State.minus = () => {
  if (State.scale > -100) {
    State.scale -= 10;
    State.recalc();
  }
};

State.change = value => {
  if (value >= -100 && value <= 100) {
    State.scale = value;
    State.recalc();
  }
};

State.recalc = () => {
  State.dom.video.style.filter = `grayscale(${grScale(
    State.scale
  )}%) contrast(${100 + State.scale / 2}%)`;
  slider.value = String((100 - State.scale) / 2);
  let c = backgroundColor(State.scale);
  State.dom.root.style.backgroundColor = `rgba(${c},${c},${c},1)`;
};

export default State;
