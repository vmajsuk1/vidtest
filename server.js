const express = require('express');
const path = require('path');

const app = express();
const port = process.env.PORT || 1357;

app.use(express.static(path.resolve(__dirname, 'public')));

app.listen(port, () => console.log('The server is listening on http://localhost:' + port));