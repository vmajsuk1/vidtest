const webpack = require('webpack');
const wpConfig = require('./webpack.config');

const compiler = webpack(wpConfig);

compiler.watch({}, () => console.log('Bundle rebuilt!'));
