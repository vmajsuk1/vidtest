/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = color;
/* harmony export (immutable) */ __webpack_exports__["c"] = parseQuery;
/* harmony export (immutable) */ __webpack_exports__["d"] = restoreOperationsFromQuery;
/* harmony export (immutable) */ __webpack_exports__["b"] = createQueryFromState;
function color(c) {
  if (c > 255) return 255;
  if (c < 0) return 0;
  return c;
}

function parseQuery(queryStr) {
  if (!queryStr) {
    return {};
  }

  let kvs = queryStr.slice(1).split("&");
  let res = {};
  let i = 0,
    n = kvs.length;

  for (; i < n; i++) {
    let tmp = kvs[i].split("=");
    res[tmp[0]] = tmp[1];
  }

  return res;
}

function restoreOperationsFromQuery(query) {
  const operations = [];

  if (query.plus) {
    let pluses = query.plus.split(","),
      i = 0,
      n = pluses.length;

    for (; i < n; i++) {
      operations.push({
        operation: "plus",
        time: pluses[i]
      });
    }
  }

  if (query.minus) {
    let minuses = query.minus.split(","),
      i = 0,
      n = minuses.length;

    for (; i < n; i++) {
      operations.push({
        operation: "minus",
        time: minuses[i]
      });
    }
  }

  if (query.change) {
    let changes = query.change.split(","),
      i = 0,
      n = changes.length;

    for (; i < n; i++) {
      const change = changes[i].split("!");
      const time = change[0];
      const value = change[1];
      operations.push({
        operation: "change",
        time,
        value
      });
    }
  }

  operations.sort((a, b) => a.time - b.time);

  return operations;
}

function createQueryFromState(State) {
  let operations = State.allOperations || State.operations;
  let pluses = operations.filter(o => o.operation === "plus").map(o => o.time);
  let minuses = operations
    .filter(o => o.operation === "minus")
    .map(o => o.time);
  let changes = operations.filter(o => o.operation === "change");
  let qryParts = [];
  if (pluses.length > 0) {
    qryParts.push("plus=" + pluses.join(","));
  }
  if (minuses.length > 0) {
    qryParts.push("minus=" + minuses.join(","));
  }
  if (changes.length > 0) {
    qryParts.push(
      "change=" + changes.map(c => c.time + "!" + c.value).join(",")
    );
  }
  let qry = qryParts.length > 0 ? "?" + qryParts.join("&") : "";

  return qry;
}


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__state__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__eventListeners__ = __webpack_require__(3);




// setTimeout(createOnVideoEndListener(State), 5000)
__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.video.addEventListener("ended", Object(__WEBPACK_IMPORTED_MODULE_2__eventListeners__["e" /* createOnVideoEndListener */])(__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */]));
__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.plus.onclick = Object(__WEBPACK_IMPORTED_MODULE_2__eventListeners__["c" /* createOnPlusListener */])(__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.minus.onclick = Object(__WEBPACK_IMPORTED_MODULE_2__eventListeners__["b" /* createOnMinusListener */])(__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.video.crossOrigin = "Anonymous";
__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.modalButton.onclick = () => {
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.modalContainer.style.opacity = 0;
  setTimeout(() => {
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.video.play();
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.modalContainer.style.display = "none";
  }, 1000);
};

__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.slider.addEventListener("change", Object(__WEBPACK_IMPORTED_MODULE_2__eventListeners__["a" /* createOnChangeListener */])(__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */]));

__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.sharedModalButton.onclick = () => {
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.sharedModalContainer.style.opacity = 0;

  setTimeout(() => {
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.video.play();
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.sharedModalContainer.style.display = "none";
  }, 1000);
};

__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.video.onclick = e => {
  if (__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].pause) {
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.video.play();
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].pause = false;
  } else {
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.video.pause();
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].pause = true;
  }
}

document.addEventListener('keypress', e => {
  if (e.keyCode !== 32) {
    return;
  }

  if (__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].pause) {
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.video.play();
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].pause = false;
  } else {
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.video.pause();
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].pause = true;
  }
})

__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.shareSiteButton.onclick = e => {
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.shareSiteButton.style.display = 'none';
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.vk2.style.display = 'block';
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.fb2.style.display = 'block';
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.url2.style.display = 'block';
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.vk2.onclick = e => {
    window.location.replace(
      "http://vk.com/share.php?url=" + encodeURIComponent(window.location.origin),
      window
    );
  };
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.fb2.onclick = e => {
    window.location.replace(
      "https://www.facebook.com/sharer.php?u=" + encodeURIComponent(window.location.origin),
      window
    );
  };
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.url2.value = window.location.origin;
}

if (window.location.search) {
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.modalContainer.style.display = "none";
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.sharedModalContainer.style.display = "flex";
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.plus.style.display = "none";
  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.minus.style.display = "none";

  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].operations = Object(__WEBPACK_IMPORTED_MODULE_1__utils__["d" /* restoreOperationsFromQuery */])(__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].query);

  __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.video.addEventListener(
    "timeupdate",
    Object(__WEBPACK_IMPORTED_MODULE_2__eventListeners__["d" /* createOnTimeUpdateListener */])(__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */])
  );

  const passMyOwnRow = document.getElementById("pass-my-own");
  const passMyOwn = document.getElementById("start-own");

  passMyOwnRow.style.display = "flex";
  passMyOwn.addEventListener("click", () =>
    window.location.replace(window.location.origin, window)
  );
}

function drawCanv() {
  if (__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].w && __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].h) {
    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.canvTmpContext.drawImage(__WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.video, 0, 0, __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].w, __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].h);

    const pixelData = __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.canvTmpContext.getImageData(
      0,
      0,
      __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].w,
      __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].h
    );

    for (let i = 0; i < pixelData.data.length; i++) {
      if (i % 4 === 3) continue;
      let updatedValue = pixelData.data[i] + __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].scale;

      pixelData.data[i] = Object(__WEBPACK_IMPORTED_MODULE_1__utils__["a" /* color */])(updatedValue);
    }

    __WEBPACK_IMPORTED_MODULE_0__state__["a" /* default */].dom.canvOutContext.putImageData(pixelData, 0, 0);
  }

  requestAnimationFrame(drawCanv);
}

// drawCanv();


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__(0);


const video = document.getElementById("vid");
const root = document.getElementById("root");
const plus = document.getElementById("plus");
const minus = document.getElementById("minus");
const buttonsRow = document.getElementById("buttons-row");
const shareButtonsRow = document.getElementById("share-buttons-row");
const shareButton = document.getElementById("share");
const urlInputRow = document.getElementById("url-row");
const urlInput = document.getElementById("url");
const slider = document.getElementById("slider");
const modalButton = document.getElementById("modal-button");
const sharedModalButton = document.getElementById("shared-modal-button");
const modalContainer = document.getElementById("modal-container");
const sharedModalContainer = document.getElementById("shared-modal-container");
const socialNetworksRow = document.getElementById("social-networks-row");
const vk = document.getElementById("vk");
const fb = document.getElementById("fb");
const sliderRow = document.getElementById("slider-row");
const watch = document.getElementById("watch");
const shareSiteButton = document.getElementById("share-site-button");
const vk2 = document.getElementById("vk-2");
const fb2 = document.getElementById("fb-2");
const url2 = document.getElementById("url-2");
const shareSite = document.getElementById("share-site");

const State = {
  scale: 0,
  w: 0,
  h: 0,
  operations: [],
  pause: false,
  dom: {
    video,
    root,
    plus,
    minus,
    buttonsRow,
    shareButtonsRow,
    shareButton,
    urlInputRow,
    urlInput,
    slider,
    modalButton,
    sharedModalButton,
    modalContainer,
    sharedModalContainer,
    socialNetworksRow,
    vk,
    fb,
    sliderRow,
    watch,
    shareSiteButton,
    vk2,
    fb2,
    url2,
    shareSite
  },
  query: Object(__WEBPACK_IMPORTED_MODULE_0__utils__["c" /* parseQuery */])(window.location.search)
};

function grScale() {
  if (State.scale === 0) {
    return 30;
  } else if (State.scale > 0) {
    return 30 - State.scale / 100 * 30;
  } else if (State.scale < 0) {
    return 30 - State.scale / 100 * 70;
  }
}

function backgroundColor(scale) {
  if (scale >= 0) {
    return 244;
  } else {
    return 244 + scale;
  }
}

State.plus = () => {
  if (State.scale < 100) {
    State.scale += 10;
    State.recalc();
  }
};

State.minus = () => {
  if (State.scale > -100) {
    State.scale -= 10;
    State.recalc();
  }
};

State.change = value => {
  if (value >= -100 && value <= 100) {
    State.scale = value;
    State.recalc();
  }
};

State.recalc = () => {
  State.dom.video.style.filter = `grayscale(${grScale(
    State.scale
  )}%) contrast(${100 + State.scale / 2}%)`;
  slider.value = String((100 - State.scale) / 2);
  let c = backgroundColor(State.scale);
  State.dom.root.style.backgroundColor = `rgba(${c},${c},${c},1)`;
};

/* harmony default export */ __webpack_exports__["a"] = (State);


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__(0);


const createShareButtonClickListener = State => {
  return () => {
    const qry = Object(__WEBPACK_IMPORTED_MODULE_0__utils__["b" /* createQueryFromState */])(State);
    const url = window.location.origin + qry;

    State.dom.sliderRow.style.display = "none";
    State.dom.shareButtonsRow.style.marginTop = "30";
    State.dom.urlInputRow.style.display = "flex";
    State.dom.urlInput.value = url;

    State.dom.socialNetworksRow.style.display = "flex";
    State.dom.vk.onclick = () =>
      window.location.replace(
        "http://vk.com/share.php?url=" + encodeURIComponent(url),
        window
      );
    State.dom.fb.onclick = () =>
      window.location.replace(
        "https://www.facebook.com/sharer.php?u=" + encodeURIComponent(url),
        window
      );

    console.log("??");

    State.dom.urlInput.select();
  };
};
/* unused harmony export createShareButtonClickListener */


const createOnPlusListener = State => {
  return () => {
    State.operations.push({
      time: State.dom.video.currentTime,
      operation: "plus"
    });
    State.plus();
  };
};
/* harmony export (immutable) */ __webpack_exports__["c"] = createOnPlusListener;


const createOnMinusListener = State => {
  return () => {
    State.operations.push({
      time: State.dom.video.currentTime,
      operation: "minus"
    });
    State.minus();
  };
};
/* harmony export (immutable) */ __webpack_exports__["b"] = createOnMinusListener;


const createOnChangeListener = State => {
  return e => {
    const scale = 100 - 2 * e.target.value;

    State.operations.push({
      time: State.dom.video.currentTime,
      operation: "change",
      value: scale
    });
    State.change(scale);
  };
};
/* harmony export (immutable) */ __webpack_exports__["a"] = createOnChangeListener;


const createOnTimeUpdateListener = State => {
  return () => {
    let i = 0,
      n = State.operations.length - 1,
      curTime = State.dom.video.currentTime;

    if (n < 0) return;

    for (; i < n; i++) {
      if (
        curTime >= State.operations[i].time &&
        curTime < State.operations[i + 1].time
      ) {
        switch (State.operations[i].operation) {
          case "plus":
            State.plus();
            break;
          case "minus":
            State.minus();
            break;
          case "change":
            State.change(State.operations[i].value);
            break;
        }

        State.operations.splice(i, 1);
      }
    }

    let newN = State.operations.length - 1;

    if (curTime >= State.operations[newN].time) {
      State.operations[newN].operation === "plus"
        ? State.plus()
        : State.minus();
      State.operations.splice(newN, 1);
    }
  };
};
/* harmony export (immutable) */ __webpack_exports__["d"] = createOnTimeUpdateListener;


const createOnVideoEndListener = State => {
  return () => {
    if (!window.location.search) {
      State.dom.shareSite.style.display = 'none';
      State.dom.buttonsRow.style.display = "none";
      State.dom.shareButtonsRow.style.display = "flex";
      State.dom.shareButton.addEventListener(
        "click",
        createShareButtonClickListener(State)
      );
      State.dom.watch.addEventListener("click", () => {
        State.allOperations = [].concat(
          State.allOperations || State.operations
        );
        State.operations = [].concat(State.allOperations);

        State.scale = 0;
        State.dom.video.currentTime = 0;
        State.dom.video.addEventListener(
          "timeupdate",
          createOnTimeUpdateListener(State)
        );
        State.recalc();
        State.dom.video.play();
      });
    }
  };
};
/* harmony export (immutable) */ __webpack_exports__["e"] = createOnVideoEndListener;


const createOnVideoCanPlayListener = State => {
  return () => {
    State.dom.video.play();
  };
};
/* unused harmony export createOnVideoCanPlayListener */



/***/ })
/******/ ]);